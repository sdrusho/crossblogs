package com.crossover.techtrial.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.crossover.techtrial.model.Article;

@RepositoryRestResource(exported = false)
public interface ArticleRepository extends PagingAndSortingRepository<Article, Long> {

  
  @Query("SELECT a FROM Article a WHERE " +
          "LOWER(a.title) LIKE "+
		  "LOWER(CONCAT('%',:title, '%')) OR " +
          "LOWER(a.content) "+
		  "LIKE LOWER(CONCAT('%',:content, '%')) "+
          "ORDER BY a.title ASC")
  List<Article> findTop10ByTitleContainingIgnoreCaseOrContentContainingIgnoreCase(@Param("title")String title,
		  @Param("content")String content,Pageable pageable);

}
