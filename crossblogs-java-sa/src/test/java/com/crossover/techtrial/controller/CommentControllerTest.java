package com.crossover.techtrial.controller;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.crossover.techtrial.model.Article;
import com.crossover.techtrial.model.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CommentControllerTest {
	@Autowired
	private TestRestTemplate template;

	@Before
	public void setup() throws Exception {

	}

	@Test
	public void testCommentControllerCreateAndGet() throws Exception {
		HttpEntity<Object> article = getHttpEntity("{\"email\": \"user2@gmail.com\", \"title\": \"hello4\" }");
		ResponseEntity<Article> resultAsset = template.postForEntity("/articles", article, Article.class);
		Assert.assertNotNull(resultAsset.getBody().getId());

		HttpEntity<Object> comment = getHttpEntity("{\"email\": \"user3@gmail.com\", \"message\": \"hello3\" }");
		ResponseEntity<Comment> response = template
				.postForEntity("/articles/" + resultAsset.getBody().getId() + "/comments", comment, Comment.class);
		Assert.assertNotNull(response.getBody().getId());

		// get test
		ParameterizedTypeReference<List<Comment>> listOfStrings = new ParameterizedTypeReference<List<Comment>>() {
		};

		ResponseEntity<List<Comment>> resultAssetGet = template.exchange(
				"/articles/" + resultAsset.getBody().getId() + "/comments/", HttpMethod.GET, null, listOfStrings);

		Assert.assertNotNull(resultAssetGet.getBody().get(0).getId());

	}

	private HttpEntity<Object> getHttpEntity(Object body) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<Object>(body, headers);
	}
}
