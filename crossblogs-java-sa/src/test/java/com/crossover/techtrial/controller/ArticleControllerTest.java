package com.crossover.techtrial.controller;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponentsBuilder;

import com.crossover.techtrial.model.Article;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ArticleControllerTest {

	@Autowired
	private TestRestTemplate template;

	@Before
	public void setup() throws Exception {

	}

	@Test
	public void testArticleShouldBeCreated() throws Exception {
		HttpEntity<Object> article = getHttpEntity("{\"email\": \"user1@gmail.com\", \"title\": \"hello\" }");
		ResponseEntity<Article> resultAsset = template.postForEntity("/articles", article, Article.class);
		Assert.assertNotNull(resultAsset.getBody().getId());
	}

	@Test
	public void testArticleUpadatedAndGet() throws Exception {
		HttpEntity<Object> article = getHttpEntity(
				"{\"email\": \"user1@gmail.com\", \"title\": \"hello_created1\",\"content\": \"content_create2\" }");
		ResponseEntity<Article> resultAsset = template.postForEntity("/articles", article, Article.class);
		Assert.assertNotNull(resultAsset.getBody().getId());
		// updated test
		HttpEntity<Object> articleUpdated = getHttpEntity(
				"{\"email\": \"user2@gmail.com\", \"title\": \"hello_updated\",\"content\": \"content_updated\" }");
		ResponseEntity<Article> responseEntity = template.exchange("/articles/" + resultAsset.getBody().getId(),
				HttpMethod.PUT, articleUpdated, Article.class);
		String[] actual = { "hello_updated" };
		String[] getTitle = { responseEntity.getBody().getTitle() };
		Assert.assertArrayEquals(actual, getTitle);
		// get test
		ResponseEntity<Article> resultAssetGet = template.getForEntity("/articles/" + resultAsset.getBody().getId(),
				Article.class);
		Assert.assertNotNull(resultAssetGet.getBody().getId());

	}

	@Test
	public void testArticleCreatedAndUpdatedAndGetAndDelete() throws Exception {
		HttpEntity<Object> article = getHttpEntity(
				"{\"email\": \"user2@gmail.com\", \"title\": \"hello_created2\",\"content\": \"content_create2\" }");
		ResponseEntity<Article> resultAsset = template.postForEntity("/articles", article, Article.class);
		Assert.assertNotNull(resultAsset.getBody().getId());
		// updated test
		HttpEntity<Object> articleUpdated = getHttpEntity(
				"{\"email\": \"user2@gmail.com\", \"title\": \"hello_updated\",\"content\": \"content_updated\" }");

		ResponseEntity<Article> responseEntity = template.exchange("/articles/" + resultAsset.getBody().getId(),
				HttpMethod.PUT, articleUpdated, Article.class);
		String[] actual = { "hello_updated" };
		String[] getTitle = { responseEntity.getBody().getTitle() };
		Assert.assertArrayEquals(actual, getTitle);
		// get test
		ResponseEntity<Article> resultAssetGet = template.getForEntity("/articles/" + resultAsset.getBody().getId(),
				Article.class);
		Assert.assertNotNull(resultAssetGet.getBody().getId());

		template.delete("/articles/" + resultAsset.getBody().getId(), Article.class);

	}
	@Test
	public void testArticleCreatedAndSearch() throws Exception {
		HttpEntity<Object> article = getHttpEntity(
				"{\"email\": \"user3@gmail.com\", \"title\": \"hello_created3\",\"content\": \"content_create3\" }");
		ResponseEntity<Article> resultAsset = template.postForEntity("/articles", article, Article.class);
		Assert.assertNotNull(resultAsset.getBody().getId());
		
		
		ParameterizedTypeReference<List<Article>> listOfStrings = new ParameterizedTypeReference<List<Article>>() {
		};

		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("/articles/search")
		        .queryParam("text", "hello");
		HttpEntity<?> entity = new HttpEntity<>(headers);
		ResponseEntity<List<Article>> resultAssetSearch = template.exchange(
				builder.toUriString(), HttpMethod.GET, entity, listOfStrings);

		Assert.assertNotNull(resultAssetSearch.getBody().get(0).getId());
	}

	private HttpEntity<Object> getHttpEntity(Object body) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<Object>(body, headers);
	}
}
